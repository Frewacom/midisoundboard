#include "AudioSelectionScreen.h"

void DisplayAudioSelectionScreen(AudioPlayer *player, ConfigurationParser *parser) {
	PrintTitle("Available devices");
	player->GetDevices();
	PrintNewLine();

	int selectedOutputDevice;

	while (true) {
		PrintPrompt("Select output device: ");
		std::cin >> selectedOutputDevice;

		if (selectedOutputDevice >= 0 && selectedOutputDevice <= player->DeviceCount - 1) {
			PrintNewLine();
			const char* id = player->DeviceIdList.at(selectedOutputDevice);
			player->CreateDevices(id);
			parser->SetDefaultAudioDeviceId(id);
			break;
		}
	}

	system("cls");
}