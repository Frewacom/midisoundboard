#include "MidiSelectionScreen.h"

void DisplayMidiSelectionScreen(MidiHandler *midi, ConfigurationParser *parser) {
	PrintTitle("MIDI-devices");
	midi->GetInputDevices();
	PrintNewLine();

	int selectedMidiDevice;

	while (true) {
		PrintPrompt("Select midi device: ");
		std::cin >> selectedMidiDevice;

		if (selectedMidiDevice >= 0 && selectedMidiDevice <= midi->InputDeviceCount - 1) {
			midi->OpenPort(selectedMidiDevice);
			parser->SetDefaultMidiDeviceId(midi->Input->getPortName(selectedMidiDevice));
			break;
		}
	}



	system("cls");
}