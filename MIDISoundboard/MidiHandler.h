#pragma once
#include <Windows.h>
#include <iostream>
#include <string>
#include <vector>
#include "RtMidi.h"
#include "helpers.h"

class MidiHandler
{
public:
	MidiHandler();
	~MidiHandler();

	RtMidiIn *Input;
	RtMidiOut *Output;
	unsigned int InputDeviceCount;
	unsigned int OutputDeviceCount;
	std::vector<std::string> InputDevices;
	std::vector<std::string> OutputDevices;

	bool HasOutput = false;
	int ActiveKey;
	std::vector<std::string> BoundKeys;

	void GetInputDevices(bool log = true);
	void GetOutputDevices();
	void OpenPort(int port);
	void LightUpBoundKeys();
	void LightOffBoundKeys();
	int GetPortIdByName(std::string name);
	void SendData(char status, char key, char byte3);

private:
	void printErrorAndDie(RtMidiError &error);
};

