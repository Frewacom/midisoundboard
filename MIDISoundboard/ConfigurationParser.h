#pragma once
#include <fstream>
#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <Windows.h>
#include <ostream>
#include "include\json.h"
#include "helpers.h"

using json = nlohmann::json;

#define VALIDATE_STRING 1
#define VALIDATE_BOOL 2
#define VALIDATE_INT 3

#define AUDIO_OUT_VOLUME_KEY "audio_out_volume"
#define AUDIO_SELF_VOLUME_KEY "audio_self_volume"
#define DEFAULT_AUDIO_DEVICE_KEY "default_audio_device_id"
#define DEFAULT_MIDI_DEVICE_KEY "default_midi_device_id"

#define TRACKS_LIST_KEY "tracks"
#define EFFECTS_LIST_KEY "effects"

#define SETTINGS_MAIN_PATH "settings/main.json"
#define SETTINGS_BINDINGS_PATH "settings/bindings.json"

class ConfigurationParser
{
public:
	ConfigurationParser();
	~ConfigurationParser();

	json Settings;
	json Bindings;

	void ParseConfig();
	bool ValidateValue(json *value, int type);
	void UpdateJSONFile(std::string file);
	void SetDefaultAudioDeviceId(std::string id);
	void SetDefaultMidiDeviceId(std::string id);
	void SetAudioVolume(std::string jsonKey, int volume);
	void OnVolumeChange(void(*callback)());
	void AddBinding(std::string file, unsigned int key, std::string list = TRACKS_LIST_KEY);
	void RemoveBinding(unsigned int key, std::string list = TRACKS_LIST_KEY);
	std::string GetSongByKey(std::string key);
	std::string GetEffectByKey(std::string key);
	std::string GetDefaultAudioDeviceId();
	std::string GetDefaultMidiDeviceId();
	std::vector<std::string> GetBoundKeys();
	int GetAudioVolume(std::string jsonKey);
	bool IsInDebug();

private:
	void(*volumeCallback)() = nullptr;
};

