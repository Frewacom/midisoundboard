#include "AudioPlayer.h"

void AudioPlayer::GetDevices() {
	ISoundDeviceList *Devices = createSoundDeviceList();

	this->DeviceCount = Devices->getDeviceCount();
	this->DeviceIdList.reserve(this->DeviceCount);

	const char *deviceName;
	const char *deviceId;

	for (int i = 0; i < this->DeviceCount; i++) {
		deviceName = Devices->getDeviceDescription(i);
		deviceId = Devices->getDeviceID(i);

		this->DeviceIdList.push_back(deviceId);
		this->DeviceNameList.push_back(deviceName);

		PrintIndex(i, "Device: " + (std::string)deviceName);
	}

	std::cout << "Number of devices found: " << this->DeviceCount << std::endl;
}

void AudioPlayer::CreateDevices(const char* deviceId) {		
	this->Output = createIrrKlangDevice(
		ESOD_AUTO_DETECT,
		ESEO_DEFAULT_OPTIONS | ESEO_PRINT_DEBUG_INFO_TO_DEBUGGER,
		deviceId
	);

	this->OutputSelf = createIrrKlangDevice(
		ESOD_AUTO_DETECT,
		ESEO_DEFAULT_OPTIONS | ESEO_PRINT_DEBUG_INFO_TO_DEBUGGER
	);

	if (this->Output == NULL) {
		PrintErrorAndDie("[ERROR] Could not create sound engine - Please check your device-settings");
	}
}

void AudioPlayer::StartPlayback(std::string file, void(*callback)(), int volumeOut, int volumeSelf) {
	const char* file_c = file.c_str();

	this->Output->setSoundVolume(((float)volumeOut / (float)100));
	this->OutputSelf->setSoundVolume(((float)volumeSelf / (float)100));

	ISound *audioSelf = this->Output->play2D(file_c, false, false, true, ESM_AUTO_DETECT, false);
	ISound *audioOut = this->OutputSelf->play2D(file_c, false, false, true, ESM_AUTO_DETECT, false);

	this->SoundOut = audioOut;
	this->SoundSelf = audioSelf;

	if (audioOut == 0) {
		std::cout << "[ERROR] Could not play file: " << file_c << std::endl;
		return;
	}
	else {
		this->IsPlaying = true;
		this->ShouldPlay = true;
	}

	while (this->Output->isCurrentlyPlaying(file_c) && this->ShouldPlay) {
		Sleep(1);
	}

	if (this->ShouldPlay) {
		this->ShouldDispose = true;
	}

	if (callback != nullptr) {
		callback();
	}

	if (audioSelf != nullptr) {
		audioSelf->drop();
		this->SoundSelf = nullptr;
	}
	if (audioOut != nullptr) {
		audioOut->drop();
		this->SoundOut = nullptr;
	}
	this->Output->stopAllSounds();
	this->OutputSelf->stopAllSounds();
	this->IsPlaying = false;
	this->ShouldPlay = false;
}

void AudioPlayer::StartParallelPlayback(std::string fileName, unsigned int key, int volumeOut, int volumeSelf) {
	const char* file_c = fileName.c_str();

	for (int i = 0; i < this->currentParallelKeys.size(); i++) {
		if (this->currentParallelKeys.at(i) == key) {
			this->CleanupParallelPlayback(file_c);
		}
	}

	ISound *audioOut = this->Output->play2D(file_c, false, false, true);
	ISound *audioSelf = this->OutputSelf->play2D(file_c, false, false, true);
}

void AudioPlayer::CleanupParallelPlayback(const char* name) {
	this->Output->removeSoundSource(name);
	this->OutputSelf->removeSoundSource(name);
}

void AudioPlayer::UpdatePlayPosition(ik_u32 move) {
	if (this->SoundOut != nullptr && this->SoundSelf != nullptr) {
		ik_u32 length = this->SoundOut->getPlayLength();
		ik_u32 lengthSelf = this->SoundSelf->getPlayLength();
		ik_u32 currentPos = this->SoundOut->getPlayPosition();
		ik_u32 currentPosSelf = this->SoundSelf->getPlayPosition();
		ik_u32 change = currentPos + move;
		ik_u32 changeSelf = currentPosSelf + move;

		if (length != -1 && lengthSelf != -1 && currentPos != -1 && currentPosSelf != -1) {
			if (move < 0) {
				change = currentPos - move;
				changeSelf = currentPosSelf - move;
			}

			if (change <= length && change >= 0) {
				this->SoundOut->setPlayPosition(change);
				return;
			}

			if (changeSelf <= lengthSelf && changeSelf >= 0) {
				this->SoundSelf->setPlayPosition(changeSelf);
				return;
			}

			std::cout << "[ERROR] Could not fast forward. Current position would reach beyond end of track" << std::endl;
		}
		else {
			std::cout << "[ERROR] Could not fast forward. Unknown length of track" << std::endl;
		}
	}
}

void AudioPlayer::DeleteDevices() {
	if (this->Output != nullptr) {
		this->Output->drop();
	}

	if (this->OutputSelf != nullptr) {
		this->OutputSelf->drop();
	}
}

AudioPlayer::~AudioPlayer()
{
	this->DeleteDevices();
}
