#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <windows.h>
#include <thread>
#include "include\irrKlang.h"
#include "helpers.h"

using namespace irrklang;

#pragma comment(lib, "irrKlang.lib")

class AudioPlayer
{
public:
	~AudioPlayer();

	ISoundEngine *Output;
	ISoundEngine *OutputSelf;
	ISound *SoundOut = nullptr;
	ISound *SoundSelf = nullptr;
	ISoundDeviceList *Devices;
	bool IsPlaying = false;
	bool ShouldPlay = false;
	bool ShouldDispose = false;
	int DeviceCount;
	std::thread AudioThread;
	std::vector<const char*> DeviceIdList;
	std::vector<const char*> DeviceNameList;

	void GetDevices();
	void CreateDevices(const char* deviceId = nullptr);
	void StartPlayback(std::string fileName, void(*callback)(), int volumeOut, int volumeSelf);
	void StartParallelPlayback(std::string fileName, unsigned int key, int volumeOut, int volumeSelf);
	void CleanupParallelPlayback(const char* name);
	void UpdatePlayPosition(ik_u32 move);
	void DeleteDevices();

private:
	std::vector<unsigned int> currentParallelKeys;
};