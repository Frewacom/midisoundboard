#include "helpers.h"
#include "rang.h"

using namespace std;
using namespace rang;

void PrintTitle(std::string title) {
	cout << style::bold << bg::blue << fgB::cyan << "[CONFIG] "
		 << title << style::reset << fg::reset << bg::reset << endl;
	PrintNewLine();
}

void PrintPrompt(std::string prompt) {
	cout << style::bold << prompt << style::reset;
}

void PrintCursive(std::string text) {
	cout << style::italic << text << style::reset << endl;
}

void PrintError(std::string error) {
	cout << fgB::red << error << fg::reset << endl;
}

void PrintSuccess(std::string text) {
	cout << style::bold << fg::green << text << fg::reset << style::reset << endl;
}

void PrintInfo(std::string text) {
	cout << fgB::yellow << text << fg::reset << endl;
}

void PrintAudioStatus(std::string text) {
	cout << "[" << style::bold << fgB::cyan << "AUDIO" << style::reset << fg::reset << "] " << text << endl;
}

void PrintIndex(int index, std::string value) {
	cout << style::bold << fg::cyan << "<" << index << "> " << fg::reset << style::reset << value << endl;
}

void PrintErrorAndDie(std::string text) {
	PrintError(text);
	Sleep(5000);
	exit(EXIT_FAILURE);
}

void PrintNewLine() {
	cout << "\n";
}