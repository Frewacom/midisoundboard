#pragma once
#include "MidiHandler.h"
#include "ConfigurationParser.h"
#include "helpers.h"

void DisplayMidiSelectionScreen(MidiHandler *midi, ConfigurationParser *parser);