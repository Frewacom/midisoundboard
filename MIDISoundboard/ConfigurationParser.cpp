#include "ConfigurationParser.h"

ConfigurationParser::ConfigurationParser()
{
	this->ParseConfig();
}

void ConfigurationParser::ParseConfig() {
	// Add in error handling for File not found etc
	std::ifstream main(SETTINGS_MAIN_PATH);
	std::ifstream binds(SETTINGS_BINDINGS_PATH);

	try {
		this->Settings = json::parse(main);
		this->Bindings = json::parse(binds);
	}
	catch (json::parse_error &error) {
		PrintErrorAndDie(error.what());
	}

	PrintSuccess("[CONFIG] Loaded settings and bindings successfully");
}

bool ConfigurationParser::ValidateValue(json *value, int type) {
	if (!value->empty()) {
		bool correctType;

		switch (type) {
			case VALIDATE_STRING:
				correctType = value->is_string();
				break;
			case VALIDATE_BOOL:
				correctType = value->is_boolean();
				break;
			case VALIDATE_INT:
				correctType = value->is_number_integer();
				break;
		}

		if (correctType) {
			return value;
		}

		return false;
	}

	return false;
}

std::string ConfigurationParser::GetSongByKey(std::string key) {
	bool isTrack = this->ValidateValue(&this->Bindings[TRACKS_LIST_KEY][key], VALIDATE_STRING);

	if (isTrack) {
		return this->Bindings[TRACKS_LIST_KEY][key];
	}

	return "";
}

std::string ConfigurationParser::GetEffectByKey(std::string key) {
	bool isEffect = this->ValidateValue(&this->Bindings[EFFECTS_LIST_KEY][key], VALIDATE_STRING);

	if (isEffect) {
		return this->Bindings[EFFECTS_LIST_KEY][key];
	}

	return "";
}

bool ConfigurationParser::IsInDebug() {
	bool isCorrect = this->ValidateValue(&this->Settings["debug"], VALIDATE_BOOL);

	if (isCorrect) {
		return this->Settings["debug"];
	}

	return false;
}

std::string ConfigurationParser::GetDefaultAudioDeviceId() {
	bool isCorrect = this->ValidateValue(&this->Settings[DEFAULT_AUDIO_DEVICE_KEY], VALIDATE_STRING);

	if (isCorrect) {
		return this->Settings[DEFAULT_AUDIO_DEVICE_KEY];
	}

	return "";
}

std::string ConfigurationParser::GetDefaultMidiDeviceId() {
	bool isCorrect = this->ValidateValue(&this->Settings[DEFAULT_MIDI_DEVICE_KEY], VALIDATE_STRING);

	if (isCorrect) {
		return this->Settings[DEFAULT_MIDI_DEVICE_KEY];
	}

	return "";
}

int ConfigurationParser::GetAudioVolume(std::string jsonKey) {
	bool isCorrect = this->ValidateValue(&this->Settings[jsonKey], VALIDATE_INT);

	if (isCorrect) {
		return this->Settings[jsonKey];
	}

	return 100;
}

std::vector<std::string> ConfigurationParser::GetBoundKeys() {
	std::vector<std::string> keys;

	for (auto it : this->Bindings.items()) {
		keys.push_back(it.key());
	}

	return keys;
}

void ConfigurationParser::UpdateJSONFile(std::string file) {
	std::ofstream f(file);

	if (file == SETTINGS_MAIN_PATH) {
		f << std::setw(4) << this->Settings << std::endl;
	}
	else {
		f << std::setw(4) << this->Bindings << std::endl;
	}
}

void ConfigurationParser::OnVolumeChange(void(*callback)()) {
	this->volumeCallback = callback;
}

void ConfigurationParser::SetDefaultAudioDeviceId(std::string id) {
	this->Settings[DEFAULT_AUDIO_DEVICE_KEY] = id;

	this->UpdateJSONFile(SETTINGS_MAIN_PATH);
}

void ConfigurationParser::SetDefaultMidiDeviceId(std::string id) {
	this->Settings[DEFAULT_MIDI_DEVICE_KEY] = id.substr(0, id.size() - 2);

	this->UpdateJSONFile(SETTINGS_MAIN_PATH);
}

void ConfigurationParser::SetAudioVolume(std::string jsonKey, int volume) {
	this->Settings[jsonKey] = volume;

	this->UpdateJSONFile(SETTINGS_MAIN_PATH);

	if (this->volumeCallback != nullptr) {
		this->volumeCallback();
	}
}

void ConfigurationParser::AddBinding(std::string file, unsigned int key, std::string list) {
	this->Bindings[list][std::to_string(key)] = file;

	this->UpdateJSONFile(SETTINGS_BINDINGS_PATH);
}

void ConfigurationParser::RemoveBinding(unsigned int key, std::string list) {
	this->Bindings[list].erase(std::to_string(key));

	this->UpdateJSONFile(SETTINGS_BINDINGS_PATH);
}

ConfigurationParser::~ConfigurationParser()
{
}
