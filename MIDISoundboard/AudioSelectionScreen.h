#pragma once
#include <iostream>
#include "AudioPlayer.h"
#include "ConfigurationParser.h"
#include "helpers.h"

using namespace irrklang;

void DisplayAudioSelectionScreen(AudioPlayer *player, ConfigurationParser *parser);
