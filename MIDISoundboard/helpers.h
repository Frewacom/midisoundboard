#pragma once
#include <string>

void PrintTitle(std::string title);
void PrintSuccess(std::string text);
void PrintPrompt(std::string prompt);
void PrintNewLine();
void PrintCursive(std::string text);
void PrintError(std::string error);
void PrintInfo(std::string text);
void PrintErrorAndDie(std::string text);
void PrintAudioStatus(std::string text);
void PrintIndex(int index, std::string value);