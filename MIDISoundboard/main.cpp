#include <iostream>
#include <string>
#include <vector>
#include <Windows.h>
#include <thread>

#include "include\irrKlang.h"
#include "helpers.h"

#include "MidiHandler.h"
#include "AudioPlayer.h"
#include "ConfigurationParser.h"
#include "AudioSelectionScreen.h"
#include "MidiSelectionScreen.h"
#include "TrackBindingScreen.h"

// TODO:
// 1. Add a separate bind-type which runs in it's own thread and plays at the same time as another track.
// 2. Consider moving the audioThread and related vars and functions to a separate class.
//    Doing so, we can simply create a new WorkerThread instance and assign it to a variable
//    in this file and let the old one finish up without blocking our program. 
// 3. Add a screen-manager class so that we can move logic from main to a separate class
//
// Apparently, irrKlang has an event-based system for detecting when audio has finished playing:
// https://www.ambiera.com/irrklang/docu/index.html#concept

AudioPlayer *audioHandler;
MidiHandler *midiHandler;
ConfigurationParser *parser;

ISoundEngine *audioSelf = nullptr;
ISoundEngine *audioOut = nullptr;

std::thread audioThread;
bool hasActiveAudioThread = false;
bool hasPaused = false;
bool printAudioStatus = true;
char currentKey = 0;
std::string currentSong = "";
// This should also move to the AudioHandler class
int volumeOut = 100;
int volumeSelf = 100;

bool isInBindMode = false;
bool isLookingForKey = false;
bool hasGottenKey = false;
unsigned int recievedBindKey = -1;

void SongFinishedCallback() {
	if (hasActiveAudioThread) {
		midiHandler->SendData(128, currentKey, 0);
		currentKey = 0;
		currentSong = "";
		if (audioHandler->ShouldPlay) {
			if (printAudioStatus) {
				PrintAudioStatus("Playback finished");
			}
		}
	}
}

// Should probably be moved to the AudioHandler class?
void ApplyVolume() {
	volumeOut = parser->GetAudioVolume(AUDIO_OUT_VOLUME_KEY);
	volumeSelf = parser->GetAudioVolume(AUDIO_SELF_VOLUME_KEY);
}

void TerminateAudioThread(char key) {
	audioHandler->ShouldPlay = false;
	audioThread.join();
	hasActiveAudioThread = false;
	if (!isInBindMode) {
		midiHandler->SendData(128, key, 0);
	}
	currentKey = 0;
	currentSong = "";
}

void SpawnAudioWorker(char key) {
	hasPaused = false;
	std::string key_str = std::to_string(key);
	std::string song = parser->GetSongByKey(key_str);

	if (!song.empty()) {
		// This fixes an issue where pressing 3 or more buttons at the same
		// time would cause the track to not stop correctly - forcing
		// us to wait for the song to finish since we call thread.join()
		// There is probably a better way to fix this and it's probably caused
		// by the way I reassign threads
		Sleep(15);

		if (hasActiveAudioThread) {
			TerminateAudioThread(key);
		}

		if (currentSong != song) {
			if (printAudioStatus) {
				PrintAudioStatus("Now playing: " + song);
			}
			audioThread = std::thread(
				&AudioPlayer::StartPlayback,
				audioHandler,
				song,
				&SongFinishedCallback,
				volumeOut,
				volumeSelf
			);
			hasActiveAudioThread = true;
			currentSong = song;
			if (currentKey != 0) {
				midiHandler->SendData(128, currentKey, 0);
			}
		}
	}
	else {
		std::string effect = parser->GetEffectByKey(key_str);

		if (!effect.empty()) {
			audioHandler->StartParallelPlayback(effect, key, volumeOut, volumeSelf);
		}
		else {
			if (parser->IsInDebug()) {
				if (printAudioStatus) {
					std::cout << "[DEBUG] The key that was pressed has no binding" << std::endl;
				}
			}
		}
	}

}

void MidiInputCallback(double deltaTime, std::vector<unsigned char> *message, void *userData) {
	if (message->size() >= 3) {
		unsigned int status = message->at(0);
		unsigned int key = message->at(1);
		unsigned int data = message->at(2);

		if (isInBindMode) {
			if (!hasGottenKey && isLookingForKey) {
				hasGottenKey = true;
				recievedBindKey = key;
				/*midiHandler->SendData(144, (char)key, 127);*/
			}

			//for (int i = 0; i < midiHandler->BoundKeys.size(); i++) {
			//	if (std::stoi(midiHandler->BoundKeys.at(i)) == (int)key) {
			//		midiHandler->SendData(144, (char)key, 127);
			//	}
			//}
		}
		else {
			if (status == 144) {
				SpawnAudioWorker(key);
			}
			else if (status == 128 && currentKey == 0) {
				if (audioHandler->IsPlaying) {
					midiHandler->SendData(144, (char)key, 127);
					currentKey = (char)key;
				}
			}
			else if (status == 224) {
				if (hasActiveAudioThread && !hasPaused) {
					hasPaused = true;
					TerminateAudioThread(key);
				}
			}
		}
	}
}

void ParallelAudioReceiverCallback(const char* name) {
	audioHandler->CleanupParallelPlayback(name);
}

void CheckIfDevicesHasBeenSaved() {
	std::string audioDeviceId = parser->GetDefaultAudioDeviceId();
	std::string midiDeviceId = parser->GetDefaultMidiDeviceId();

	if (audioDeviceId != "") {
		PrintSuccess("[CONFIG] Loaded default Audio device");
	}

	if (midiDeviceId != "") {
		PrintSuccess("[CONFIG] Loaded default MIDI device");
	}

	if (midiDeviceId == "") {
		DisplayMidiSelectionScreen(midiHandler, parser);
	}
	else {
		midiHandler->GetInputDevices(false);
		int port = midiHandler->GetPortIdByName(midiDeviceId);
		if (port != -1) {
			midiHandler->OpenPort(port);
		}
		else {
			PrintNewLine();
			PrintErrorAndDie("[ERROR] Could not open port to default MIDI-device - Check your settings");
		}
	}

	if (audioDeviceId == "") {
		DisplayAudioSelectionScreen(audioHandler, parser);
	}
	else {
		PrintNewLine();
		audioHandler->CreateDevices(audioDeviceId.c_str());
		Sleep(1000);
		system("cls");
	}
}

void onExit(void) {
	delete midiHandler;
	delete audioHandler;
	delete parser;
}

int main() {
	audioHandler = new AudioPlayer();
	midiHandler = new MidiHandler();
	parser = new ConfigurationParser();

	atexit(onExit);

	ApplyVolume();
	parser->OnVolumeChange(&ApplyVolume);
	midiHandler->BoundKeys = parser->GetBoundKeys(); 

	CheckIfDevicesHasBeenSaved();
	midiHandler->Input->setCallback(&MidiInputCallback);

	PrintInfo("Type 'tracks' for setting up binds");
	PrintInfo("Type 'effects' for setting up binds for parallel audio");
	PrintInfo("Type 'volume' to change the volume for an audio source");
	PrintInfo("Type -1 to exit a screen");
	PrintNewLine();

	while (true) {
		std::string action;
		std::cin >> action;

		if (action == "tracks") {
			system("cls");
			printAudioStatus = false;
			std::string path = "tracks/";
			int iteration = 1;
			std::vector<std::string> tracks;

			PrintTitle("Tracks found in " + path);
			PrintIndex(0, "Action: Unbind key");
			for (fs::directory_entry track : fs::directory_iterator(path)) {
				fs::path trackPath = track;
				std::string track_str = trackPath.u8string();
				tracks.push_back(track_str);

				PrintIndex(iteration, "Track: " + track_str);
				iteration++;
			}

			PrintNewLine();

			isInBindMode = true;
			bool displayPrompt = true;
			bool shouldUnbind = false;
			int selectedTrack;
			std::string selectedTrackPath;
			/*midiHandler->LightUpBoundKeys();*/

			while (isInBindMode) {
				if (displayPrompt) {
					PrintPrompt("Select track to bind: ");
					std::cin >> selectedTrack;

					if (selectedTrack >= 0 && selectedTrack < iteration) {
						displayPrompt = false;
						isLookingForKey = true;
						PrintPrompt("Press key on MIDI-device: ");

						if (selectedTrack == 0) {
							shouldUnbind = true;
						}
						else {
							selectedTrackPath = tracks.at(selectedTrack - 1);
						}
					}
					else if (selectedTrack == -1) {
						isInBindMode = false;
						isLookingForKey = false;
					}
				}

				if (hasGottenKey && isLookingForKey) {
					std::cout << recievedBindKey << std::endl;
					if (shouldUnbind) {
						parser->RemoveBinding(recievedBindKey);

						//for (int i = 0; i < midiHandler->BoundKeys.size(); i++) {
						//	if (std::stoi(midiHandler->BoundKeys.at(i)) == (int)recievedBindKey) {
						//		midiHandler->BoundKeys.erase(midiHandler->BoundKeys.begin() + i);
						//		break;
						//	}
						//}
					}
					else {
						parser->AddBinding(selectedTrackPath, recievedBindKey);
						/*midiHandler->BoundKeys.push_back(std::to_string(recievedBindKey));*/
					}
					/*midiHandler->LightUpBoundKeys();*/
					recievedBindKey = -1;
					hasGottenKey = false;
					isLookingForKey = false;
					displayPrompt = true;
					shouldUnbind = false;
				}

				Sleep(50);
			}

			/*midiHandler->LightOffBoundKeys();*/
			printAudioStatus = true;
			system("cls");
		}
		else if (action == "volume") {
			system("cls");
			printAudioStatus = false;

			int volumeOut = parser->GetAudioVolume(AUDIO_OUT_VOLUME_KEY);
			int volumeSelf = parser->GetAudioVolume(AUDIO_SELF_VOLUME_KEY);

			PrintTitle("Current volumes");
			PrintIndex(0, "Volume out: " + std::to_string(volumeOut));
			PrintIndex(1, "Volume self: " + std::to_string(volumeSelf));
			PrintNewLine();

			bool isInVolumeMode = true;
			bool displayVolumeSelectPrompt = true;
			bool displayVolumeValuePrompt = false;

			int selectedVolume;
			int selectedVolumeValue;
			std::string selectedAudioSource;

			while (isInVolumeMode) {
				if (displayVolumeSelectPrompt) {
					PrintPrompt("Select audio source: ");
					std::cin >> selectedVolume;
					if (selectedVolume >= 0 && selectedVolume <= 1) {
						if (selectedVolume == 0) {
							selectedAudioSource = AUDIO_OUT_VOLUME_KEY;
						}
						else {
							selectedAudioSource = AUDIO_SELF_VOLUME_KEY;
						}

						displayVolumeSelectPrompt = false;
						displayVolumeValuePrompt = true;

					}
					else if (selectedVolume == -1) {
						isInVolumeMode = false;
					}
				}
				else if (displayVolumeValuePrompt) {
					PrintPrompt("Choose volume (1-100): ");
					std::cin >> selectedVolumeValue;
					
					if (selectedVolumeValue >= 0 && selectedVolumeValue <= 100) {
						parser->SetAudioVolume(selectedAudioSource, selectedVolumeValue);
						displayVolumeSelectPrompt = true;
						displayVolumeValuePrompt = false;
					}
				}

				Sleep(50);
			}

			printAudioStatus = true;
			system("cls");
		}
		else if (action == "effects") {
			system("cls");
			printAudioStatus = false;
			std::string path = "effects/";
			int iteration = 1;
			std::vector<std::string> effects;

			PrintTitle("Effects found in " + path);
			PrintIndex(0, "Action: Unbind key");
			for (fs::directory_entry effect : fs::directory_iterator(path)) {
				fs::path effectPath = effect;
				std::string effect_str = effectPath.u8string();
				effects.push_back(effect_str);

				PrintIndex(iteration, "Effect: " + effect_str);
				iteration++;
			}

			PrintNewLine();

			isInBindMode = true;
			bool displayPrompt = true;
			bool shouldUnbind = false;
			int selectedEffect;
			std::string selectedEffectPath;

			while (isInBindMode) {
				if (displayPrompt) {
					PrintPrompt("Select effect to bind: ");
					std::cin >> selectedEffect;

					if (selectedEffect >= 0 && selectedEffect < iteration) {
						displayPrompt = false;
						isLookingForKey = true;
						PrintPrompt("Press key on MIDI-device: ");

						if (selectedEffect == 0) {
							shouldUnbind = true;
						}
						else {
							selectedEffectPath = effects.at(selectedEffect - 1);
						}
					}
					else if (selectedEffect == -1) {
						isInBindMode = false;
						isLookingForKey = false;
					}
				}

				if (hasGottenKey && isLookingForKey) {
					std::cout << recievedBindKey << std::endl;
					if (shouldUnbind) {
						parser->RemoveBinding(recievedBindKey, EFFECTS_LIST_KEY);
					}
					else {
						parser->AddBinding(selectedEffectPath, recievedBindKey, EFFECTS_LIST_KEY);
					}

					recievedBindKey = -1;
					hasGottenKey = false;
					isLookingForKey = false;
					displayPrompt = true;
					shouldUnbind = false;
				}

				Sleep(50);
			}

			printAudioStatus = true;
			system("cls");
		}
	}

	return 0;
}