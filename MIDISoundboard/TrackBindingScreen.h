#pragma once
#include <string>
#include <iostream>
#include <filesystem>
#include "helpers.h"

namespace fs = std::experimental::filesystem::v1;

void DisplayTrackBindingScreen(std::string path);
