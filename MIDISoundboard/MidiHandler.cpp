#include "MidiHandler.h"

MidiHandler::MidiHandler()
{
	try {
		this->Input = new RtMidiIn();
		this->Output = new RtMidiOut();
		this->GetOutputDevices();
		system("cls");
	}
	catch (RtMidiError &error) {
		PrintErrorAndDie(error.getMessage());
	}
}

void MidiHandler::GetInputDevices(bool log) {
	bool isPolling = true;
	bool hasPrintedInfo = false;
	std::string deviceName;

	while (isPolling) {
		this->InputDeviceCount = this->Input->getPortCount();

		if (this->InputDeviceCount <= 0) {
			if (!hasPrintedInfo) {
				PrintNewLine();
				PrintInfo("[MIDI] No devices was found, polling until found...");
				hasPrintedInfo = true;
			}

			Sleep(2000);
		}
		else {
			this->InputDevices.reserve(this->InputDeviceCount);

			for (unsigned int i = 0; i < this->InputDeviceCount; i++) {
				deviceName = this->Input->getPortName(i);
				this->InputDevices.push_back(deviceName.substr(0, deviceName.size() - 2));

				if (log) {
					PrintIndex(i, "Device: " + deviceName.substr(0, deviceName.size() - 2));
				}
			}

			if (log) {
				std::cout << "Number of devices found: " << this->InputDeviceCount << std::endl;
			}

			isPolling = false;
		}
	}
}

void MidiHandler::GetOutputDevices() {
	this->OutputDeviceCount = this->Output->getPortCount();
	std::string deviceName;

	this->OutputDevices.reserve(this->OutputDeviceCount);

	for (unsigned int i = 0; i < this->OutputDeviceCount; i++) {
		deviceName = this->Output->getPortName(i);
		this->OutputDevices.push_back(deviceName.substr(0, deviceName.size() - 2));
	}
}

void MidiHandler::OpenPort(int port) {
	try {
		this->Input->openPort(port);
		std::string inputName = this->InputDevices.at(port);
		std::string outputName;

		for (unsigned int i = 0; i < this->OutputDeviceCount; i++) {
			outputName = this->OutputDevices.at(i);
			if (inputName == outputName) {
				this->Output->openPort(i);
				this->HasOutput = true;
			}
		}
	}
	catch (RtMidiError &error) {
		PrintErrorAndDie(error.getMessage());
	}
}

int MidiHandler::GetPortIdByName(std::string name) {
	std::string deviceName;

	for (unsigned int i = 0; i < this->InputDeviceCount; i++) {
		deviceName = this->InputDevices.at(i);
		if (deviceName == name) {
			return i;
		}
	}

	return -1;
}

void MidiHandler::SendData(char status, char key, char byte3) {
	if (this->HasOutput) {
		std::vector<unsigned char> message;
		message.reserve(3);

		message.push_back(status);
		message.push_back(key);
		message.push_back(byte3);

		this->Output->sendMessage(&message);
	}
}

void MidiHandler::LightUpBoundKeys() {
	for (int i = 0; i < this->BoundKeys.size(); i++) {
		this->SendData(144, std::stoi(this->BoundKeys.at(i)), 127);
	}
}

void MidiHandler::LightOffBoundKeys() {
	for (int i = 0; i < this->BoundKeys.size(); i++) {
		this->SendData(128, std::stoi(this->BoundKeys.at(i)), 0);
	}
}

MidiHandler::~MidiHandler()
{
	delete this->Input;
	delete this->Output;

	this->LightOffBoundKeys();
}
