#include "TrackBindingScreen.h"

void DisplayTrackBindingScreen(std::string path) {
	system("cls");
	PrintTitle("Tracks found in '" + path + "'");
	for (fs::directory_entry track : fs::directory_iterator(path)) {
		fs::path path = track;
		std::string track_str = path.u8string();

		std::cout << track_str << std::endl;
	}
}